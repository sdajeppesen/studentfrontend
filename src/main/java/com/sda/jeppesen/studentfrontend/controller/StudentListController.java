package com.sda.jeppesen.studentfrontend.controller;

import com.sda.jeppesen.studentfrontend.service.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller // każda metoda controllera zwraca widok.
@RequestMapping("/student")
@AllArgsConstructor
public class StudentListController {

    private final StudentService studentService;

    @GetMapping("")
    // typ zwracany to string - nazwa pliku html
    public String widokListaStudentow(Model model){
        model.addAttribute("lista_studentow", studentService.callStudentServiceAndRequestList());
        return "student_list";
    }
}
