package com.sda.jeppesen.studentfrontend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentfrontendApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudentfrontendApplication.class, args);
    }

}
