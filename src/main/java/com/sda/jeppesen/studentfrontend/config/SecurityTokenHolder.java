package com.sda.jeppesen.studentfrontend.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.stereotype.Component;

/**
 * Przechowuj obecny token do aplikacji!
 */
@Getter
@Setter
@ToString
public class SecurityTokenHolder {
    private String token; // domyślnie wartość = null
}
