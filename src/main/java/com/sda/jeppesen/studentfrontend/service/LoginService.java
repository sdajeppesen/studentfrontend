package com.sda.jeppesen.studentfrontend.service;

import com.sda.jeppesen.studentfrontend.config.SecurityTokenHolder;
import com.sda.jeppesen.studentfrontend.model.LoginRequest;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

/**
 * Wykonanie logowania do drugiego serwisu (student demo).
 */
@Service
@RequiredArgsConstructor
public class LoginService {
    @Value("${service.student.backend.login}")
    private String backendLogin;

    @Value("${service.student.backend.password}")
    private String backendPassword;

    private final SecurityTokenHolder tokenHolder;
    private final RestTemplate restTemplate;

    private boolean isLoggedIn(){
        return tokenHolder.getToken() != null;
    }

    private void tryLogin(){
        ResponseEntity<Void> responseEntity = restTemplate.exchange(
                URI.create("http://localhost:8081/login"),
                HttpMethod.POST,
                getLoginRequestEntity(),
                Void.class);

        if(responseEntity.getStatusCode() == HttpStatus.OK){
            System.out.println("Udało się zalogować");
            // jeśli się uda, to powinienem otrzymać jeden nagłówek z tokenem
            tokenHolder.setToken(responseEntity.getHeaders().get("x-app-token").get(0));
        }else{
            System.out.println("NIE Udało się zalogować");
        }
    }

    public void checkLogin(){
        if(!isLoggedIn()){
            tryLogin();
        }
    }

    private HttpEntity<LoginRequest> getLoginRequestEntity(){
        return new HttpEntity<>(new LoginRequest(backendLogin, backendPassword));
    }

    public <T> HttpEntity<T> getHttpEntityWithBody(T body){
        MultiValueMap<String, String> multiValueMap = new HttpHeaders();
        multiValueMap.put("x-app-token", Collections.singletonList(tokenHolder.getToken()));

        return new HttpEntity<>(body, multiValueMap);
    }

    public HttpEntity<Void> getHttpEntity(){
        MultiValueMap<String, String> multiValueMap = new HttpHeaders();
        multiValueMap.put("x-app-token", Collections.singletonList(tokenHolder.getToken()));

        return new HttpEntity<>(multiValueMap);
    }
}
