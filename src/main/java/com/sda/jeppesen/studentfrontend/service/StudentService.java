package com.sda.jeppesen.studentfrontend.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sda.jeppesen.studentfrontend.model.StudentDTO;
import lombok.AllArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

@Service
@AllArgsConstructor
public class StudentService {
    private final RestTemplate restTemplate;
    private final LoginService loginService;

    public List<StudentDTO> callStudentServiceAndRequestList() {
        loginService.checkLogin();

        ParameterizedTypeReference<List<StudentDTO>> typeReference = new ParameterizedTypeReference<List<StudentDTO>>() {
        };

        ResponseEntity<List<StudentDTO>> response = restTemplate.exchange(
                URI.create("http://localhost:8081/student"),
                HttpMethod.GET,
                loginService.getHttpEntity(), // nagłówek logowania
                typeReference);

        return response.getBody();
    }
}
