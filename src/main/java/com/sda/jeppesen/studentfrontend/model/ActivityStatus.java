package com.sda.jeppesen.studentfrontend.model;

public enum ActivityStatus {
    NIEOBECNY,           // 0
    OBECNY,              // 1
}
